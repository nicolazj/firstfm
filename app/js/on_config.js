'use strict';

/**
 * @ngInject
 */
function OnConfig($stateProvider, $locationProvider, $urlRouterProvider) {

  $locationProvider.html5Mode(true);

  $stateProvider
  .state('Home', {
    url: '/',
    controller: 'HomeCtrl as home',
    templateUrl: 'home.html',
    title: 'Home'
  })
  .state('HomePage', {
    url: '/top/:page',
    controller: 'HomeCtrl as home',
    templateUrl: 'home.html',
    title: 'Home'
  })
  .state('ArtistDetail', {
    url:'/artist/:artist',
    controller: 'ArtistCtrl as artist',
    templateUrl: 'artist.html',
    title: 'Artist'
  })
  .state('Search', {
    url:'/search/:artist/:page',
    controller: 'SearchCtrl as search',
    templateUrl: 'search.html',
    title: 'Search'
  })
  ;



  $urlRouterProvider.otherwise('/');

}

module.exports = OnConfig;
