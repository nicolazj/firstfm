'use strict';

var servicesModule = require('./_index.js');

/**
 * @ngInject
 */
function LastFMService($q, $resource,AppSettings) {

  var service = {};
  var res = $resource(AppSettings.APIEndpoint, {
    'api_key':AppSettings.APIKey,
    'format': 'json'
  });

  service.getTopArtists = function(page = 1, limit = 12) {
    var deferred = $q.defer();

    var result = res.get({
      'method': 'chart.gettopartists',
      page: page,
      limit: limit
    }, function() {
      deferred.resolve(result.artists);
    });
    return deferred.promise;
  };

  service.getArtistInfo = function(artist) {
    var deferred = $q.defer();

    var result = res.get({
      'method': 'artist.getinfo',
      artist: artist
    }, function() {
      deferred.resolve(result.artist);
    });
    return deferred.promise;
  };

  service.searchArtists = function(artist, page = 1, limit = 12) {
    var deferred = $q.defer();

    var result = res.get({
      'method': 'artist.search',
      artist: artist,
      page: page,
      limit: limit
    }, function() {
      deferred.resolve(result.results);
    });
    return deferred.promise;
  };

  return service;

}

servicesModule.service('LastFMService', LastFMService);
