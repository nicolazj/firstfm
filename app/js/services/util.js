'use strict';

var servicesModule = require('./_index.js');

/**
 * @ngInject
 */
function UtilService() {

  var service = {};


  service.genPaginationInfo = function(pageInfo, url) {

    var pageStart2End = function(curr_index, page_count, range) {
      var link_count = range || 5;
      var start = Math.max(1, curr_index - parseInt(link_count / 2)),
        end = Math.min(page_count, start + link_count - 1);
      start = Math.max(1, end - link_count + 1);
      return [start, end];
    };

    var currentPage = parseInt(pageInfo.page),
      totalPage = parseInt(pageInfo.total);

    var pagerData = {
      pageRange: [],
      url: url
    };

    var range = pageStart2End(currentPage, totalPage);

    if (currentPage > 1) {
      pagerData.firstPage = 1;
      pagerData.prevPage = currentPage - 1;
    }
    if (currentPage < totalPage) {
      pagerData.lastPage = totalPage;
      pagerData.nextPage = currentPage + 1;
    }
    var i = range[0];
    for (; i <= range[1]; i++) {
      pagerData.pageRange.push({
        index: i,
        selected: i === currentPage
      });
    }

    return pagerData;
  };

  return service;

}

servicesModule.service('UtilService', UtilService);
