'use strict';

var controllersModule = require('./_index');

/**
 * @ngInject
 */
function SearchCtrl($stateParams, $timeout, LastFMService, UtilService) {

  // ViewModel
  var vm = this;
  vm.loading = true;

  var artist = $stateParams.artist;
  var page = $stateParams.page || 1;

  LastFMService.searchArtists(artist, page).then(function(data) {
    vm.loading = false;

    if (data['opensearch:totalResults'] !== '0') {

      if (!Array.isArray(data.artistmatches.artist)) {
        data.artistmatches.artist = [data.artistmatches.artist];
      }

      var page = data['opensearch:Query'].startPage;
      var total = Math.ceil(data['opensearch:totalResults'] / data['opensearch:itemsPerPage']);

      vm.pageInfo = UtilService.genPaginationInfo({
        page: page,
        total: total
      }, '/search/' + artist + '/');

      vm.data = data;

    } else {
      vm.nodata = true;
    }
  });
}

controllersModule.controller('SearchCtrl', SearchCtrl);
