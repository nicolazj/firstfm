'use strict';

var controllersModule = require('./_index');

/**
 * @ngInject
 */
function HomeCtrl($stateParams, $timeout, LastFMService, UtilService) {

  // ViewModel
  var vm = this;
  vm.loading = true;

  var page = $stateParams.page || 1;

  LastFMService.getTopArtists(page).then(function(data) {
    vm.data = data;

    var page = vm.data['@attr'].page;
    var total = vm.data['@attr'].total;

    vm.pageInfo = UtilService.genPaginationInfo({
      page: page,
      total: total
    }, '/top/');

    vm.loading = false;
  });

}

controllersModule.controller('HomeCtrl', HomeCtrl);
