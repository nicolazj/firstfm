'use strict';

var controllersModule = require('./_index');

/**
 * @ngInject
 */
function NaviCtrl($location) {

  // ViewModel
  var vm = this;

  vm.search = function() {
    $location.path('/search/'+vm.artist+'/1');
  };
}

controllersModule.controller('NaviCtrl', NaviCtrl);
