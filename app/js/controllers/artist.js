'use strict';

var controllersModule = require('./_index');

/**
 * @ngInject
 */
function ArtistCtrl($stateParams, $timeout, LastFMService) {

  // ViewModel
  var vm = this;
  vm.loading = true;

  var artist = $stateParams.artist;

  LastFMService.getArtistInfo(artist).then(function(data) {
    vm.data = data;
    vm.loading = false;
  });

}

controllersModule.controller('ArtistCtrl', ArtistCtrl);
