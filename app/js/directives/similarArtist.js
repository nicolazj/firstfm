'use strict';

var directivesModule = require('./_index.js');

/**
 * @ngInject
 */
function similarArtistDirective() {

  return {
    restrict: 'E',
    scope: {
      data: '='
    },
    templateUrl: 'templates/similarartist.html'
  };

}

directivesModule.directive('similarArtist', similarArtistDirective);