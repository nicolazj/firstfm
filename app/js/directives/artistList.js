'use strict';

var directivesModule = require('./_index.js');

/**
 * @ngInject
 */
function artistListDirective() {

  return {
    restrict: 'E',
    scope: {
      data: '='
    },
    templateUrl: 'templates/artistlist.html'
  };

}

directivesModule.directive('artistList', artistListDirective);
