'use strict';

var directivesModule = require('./_index.js');

/**
 * @ngInject
 */
function preloaderDirective() {

  return {
    restrict: 'EA',
    templateUrl: 'templates/preloader.html'
  };

}

directivesModule.directive('preloader', preloaderDirective);