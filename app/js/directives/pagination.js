'use strict';

var directivesModule = require('./_index.js');

/**
 * @ngInject
 */
function paginationDirective() {

  return {
    restrict: 'E',
    scope: {
     data: '='
   },
    templateUrl:'templates/pagination.html'
  };

}

directivesModule.directive('pagination', paginationDirective);
